#Description

Miscellaneous files supporting the Early Print project.

##Contents

The *xsl* directory contains XSL scripts for manipulating the TEI files of
the corpus.

The *schemata* directory contains relax-ng files for validating the TEI files
of the corpus.
