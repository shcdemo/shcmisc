<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei"
    version="1.0">

<!--
    Example usage:

    Set current working directory to the location of the stylesheet and external vid2goid_map.xml file,
    and run something like one of the following:

    xalan -i 1 -o ../../phase1texts/texts/A01/A01947.xml.new ../../phase1texts/texts/A01/A01947.xml add_goid.xsl

    xsltproc -o ../../phase1texts/texts/A01/A01947.xml.new add_goid.xsl ../../phase1texts/texts/A01/A01947.xml

    Assumptions: 

    If we already have a publicationStmt within teiHeader that contains <idno type="PROQUESTGOID">, this
    stylesheet should just function as an identity stylesheet and do nothing.

    We depend on the existence of an idno element under publicationStmt with [@type='vid' or @type='VID'].
    The publicationStmt may be under fileDesc or sourceDesc (it's not consistent) but as long as there is
    one and only one idno element of the correct (case-insensitive) type, we'll find it.

    All children of publicationStmt except <availability> will go before the new idno insertion.
-->

  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:variable name="vid" select="//tei:teiHeader//tei:publicationStmt/tei:idno[@type='vid' or @type='VID']/text()" />

  <!-- Get publicationStmt sections -->
  <xsl:template match="//tei:teiHeader//tei:publicationStmt">
    <xsl:choose>
      <!-- Insert goid hwen missing <idno type="PROQUESTGOID"/> but having <idno type="VID"/> -->
      <xsl:when test="count(./tei:idno[@type='PROQUESTGOID']) = 0 and count(./tei:idno[@type='vid' or @type='VID']) = 1">
        <xsl:copy>
          <xsl:call-template name="copy-pre-children"/>
          <xsl:element name="idno">
            <xsl:attribute name="type">PROQUESTGOID</xsl:attribute>
            <xsl:value-of select="document('vid2goid_map.xml')//vid2goid[@vid=$vid]"/>
          </xsl:element>
          <xsl:call-template name="copy-post-children"/>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="copy-pre-children">
    <xsl:copy-of select="./*[not(self::tei:availability)]"/>
  </xsl:template>

  <xsl:template name="copy-post-children">
    <xsl:copy-of select="./tei:availability"/>
  </xsl:template>

  <!-- IDENTITY -->
  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@* | text() | comment() | processing-instruction()">
    <xsl:copy/>
  </xsl:template>

</xsl:stylesheet>
