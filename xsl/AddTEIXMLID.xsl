<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs tei" version="2.0">

<!--
    Example usage:

    saxon A11909_00.xml ../shcmisc/xsl/AddTEIXMLID.xsl -o:A11909_00.xml.new 
-->
    <xsl:variable name="TEIXMLID">
        <xsl:value-of select="substring-before(replace(base-uri(),'^.*[\\/]', ''),'.')"/>
    </xsl:variable>

    <!-- Default Identity Templates;  
         output is the same as input unless templates below override
    -->
    <xsl:template match="*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="@* | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>

    <!-- TEMPLATES -->
    <!-- Add xml:id attribute to TEI element if it doesn't have one. -->

    <xsl:template match="tei:TEI[not(@xml:id)]">
        <xsl:text>
</xsl:text>
        <xsl:copy>
            <xsl:attribute name="xml:id">
                <xsl:value-of select="$TEIXMLID"/>
            </xsl:attribute>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
        <xsl:text>
</xsl:text>
</xsl:template>

</xsl:stylesheet>
