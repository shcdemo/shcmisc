<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">

    <!-- default identity templates -->
    
    <xsl:template match="element()">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="attribute() | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>
    
    
    
    <!-- the default transformation is to infer a space after pc or w and insert '<c> </c>' -->

    <xsl:template match="tei:pc|tei:w">
        <xsl:copy>
            <xsl:apply-templates select="@*[name() ne 'join'], node()"/>
        </xsl:copy>
        <!-- but in fact, don't bother with added space unless there's a following-sibling --> 
        <xsl:if test="following-sibling::*">
            <c><xsl:text> </xsl:text></c>
        </xsl:if>
    </xsl:template>

    <!-- if <pc> has @join='right' or @join='both', or if it has no siblings after it, copy with no additional space -->
    
    <xsl:template match="tei:pc[@join='right' or @join='both']">
        <xsl:copy>
            <xsl:apply-templates select="@*[name() ne 'join'], node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- if <w> is followed by <pc> with @join='left' or @join='both', copy with no additional space -->
    
    <xsl:template match="tei:w[following::*[1][self::tei:pc[@join='left' or @join='both']]]">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
