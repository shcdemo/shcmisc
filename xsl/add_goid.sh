#!/bin/sh
alias saxon='java -jar ~/Downloads/SaxonHE10-1J/saxon-he-10.1.jar'

abspath() { old=`pwd`;new=$(dirname "$1");if [ "$new" != "." ]; then cd "$new"; fi;file=`pwd`/$(basename "$1");cd "$old";echo "$file"; }
source=$(abspath $1)

SCRIPTPATH=$(dirname "$0")
cd "$SCRIPTPATH"

tmp="$(mktemp)"

saxon -s:"$source" -o:"$tmp" -xsl:add_goid.xsl || {
  echo 'Saxon XSLT processing failed for:' "$source" >&2
  exit 1
}

mv "$tmp" "$source"

