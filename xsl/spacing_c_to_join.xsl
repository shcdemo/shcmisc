<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs" version="1.0">

    <xsl:output method="xml" version="1.0" encoding="UTF-8"/>

    <!-- IDENTITY -->
    <xsl:template match="*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="@* | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>

    <!-- delete an all-space text node right before <c> --> 
    
    <xsl:template match="text()[normalize-space(.)='' and name(following::*[1])='c']"/>
    
    <!-- drop explicit space -->
    
    <xsl:template match="tei:c"/>

    <!-- if <pc> comes right after <w>, then <pc @join='left'> -->

    <!-- this is implied and need not be made explicit
    
    <xsl:template match="tei:pc[name(preceding::*[1]) = 'w']" priority="10">
        <xsl:copy>
            <xsl:attribute name="join">left</xsl:attribute>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    -->

    <!-- if <pc> comes right before <w>, then <pc @join='right'> -->
    
    <xsl:template match="tei:pc[name(following::*[1]) = 'w']" priority="1">
        <xsl:copy>
            <xsl:attribute name="join">right</xsl:attribute>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>


</xsl:stylesheet>
