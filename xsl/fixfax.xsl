<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs tei" version="1.0">

<!--
    Example usage:

    xsltproc -o A71267.xml.new fixfacs.xsl A71267.xml 
-->
    <!-- Default Identity Templates;  
         output is the same as input unless templates below override
    -->
    <xsl:template match="*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="@* | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>

    <!-- TEMPLATES -->
    <!-- Move facs attribute value to xml:id attribute on w or pc element and drop facs. -->

    <xsl:template match="tei:w[@xml:id and @facs] | tei:pc[@xml:id and @facs]">
        <xsl:copy>
            <xsl:apply-templates select="@*[not(name() = 'facs' or name()='xml:id')]"/>
            <xsl:attribute name="xml:id">
                <xsl:value-of select="@facs"/>
            </xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
