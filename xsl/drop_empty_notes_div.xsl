<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs tei" version="1.0">

<!--
    Example usage:

    xsltproc -o A71267.xml.new drop_empty_notes_div.xsl A71267.xml 
-->
    <!-- Default Identity Templates;  
         output is the same as input unless templates below override
    -->
    <xsl:template match="*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="@* | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>

    <!-- TEMPLATES -->
    <!-- Only copy note divs in the back matter if they contain notes. -->

    <xsl:template match="tei:back/tei:div[@type='notes']">
        <xsl:if test="count(descendant::tei:note) != 0">
            <xsl:copy>
                <xsl:apply-templates select="@* | node()"/>
            </xsl:copy>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
